import psycopg2
import datetime

DATABASE_PARAMS = "dbname=test user=maldad password=pass host=localhost"
SCHEMA = "set search_path to test;"

def queryMaxFolio():
    CONNECTOR = psycopg2.connect(DATABASE_PARAMS)
    CURSOR = CONNECTOR.cursor()
    CURSOR.execute(SCHEMA)

    SQL = 'select * from test.registros order by folio desc limit 1;'
    CURSOR.execute(SQL)
    reg = CURSOR.fetchone()
    folio = reg[1]

    CONNECTOR.commit()
    CURSOR.close()
    CONNECTOR.close()
    return str(folio)

def insertRegister(json):
    CONNECTOR = psycopg2.connect(DATABASE_PARAMS)
    CURSOR = CONNECTOR.cursor()
    CURSOR.execute(SCHEMA)

    SQL = 'insert into test.registros values (%s, %s, %s, %s, %s, %s, %s, %s, %s);'
    d = json['fecha'].encode('ascii', 'ignore')
    d = d.split('/')
    print d
    CURSOR.execute(
            SQL,
            (
                datetime.date(int(d[2]), int(d[1]), int(d[0])),
                int(json['folio']),
                json['nombre'],
                json['matricula'],
                json['carrera'],
                json['concepto'],
                json['nivel'],
                json['numero'],
                json['letra']
                )
            )
    CONNECTOR.commit()
    CURSOR.close()
    CONNECTOR.close()

def queryNumberOfRecords():
    CONNECTOR = psycopg2.connect(DATABASE_PARAMS)
    CURSOR = CONNECTOR.cursor()
    CURSOR.execute(SCHEMA)

    SQL = 'select * from test.registros;'
    CURSOR.execute(SQL)
    results = CURSOR.fetchall()

    CONNECTOR.commit()
    CURSOR.close()
    CONNECTOR.close()
    return len(results)

def queryAll():
    CONNECTOR = psycopg2.connect(DATABASE_PARAMS)
    CURSOR = CONNECTOR.cursor()
    CURSOR.execute(SCHEMA)

    SQL = 'select * from test.registros;'
    CURSOR.execute(SQL)
    results = CURSOR.fetchall() #its a list of tuples

    CONNECTOR.commit()
    CURSOR.close()
    CONNECTOR.close()

    output = '{ \"results\" : ['
    counter = 0
    for i in results:
        counter = counter + 1;
        output = output + getListFromTuple(i)
        #sorry for this, an extra comma in the JSON :/
        if(counter < len(results)):
            output = output + ","
    output = output + "]}"
    return output

def getListFromTuple(tup):
    out = "{ "
    out = out + '\"fecha\" : \"' + str(tup[0]) + '\", '
    out = out + '\"folio\" : \"' + str(tup[1]) + '\", '
    out = out + '\"nombre\" : \"' + tup[2] + '\", '
    out = out + '\"matricula\" : \"' + tup[3] + '\", '
    out = out + '\"carrera\" : \"' + tup[4] + '\", '
    out = out + '\"concepto\" : \"' + tup[5] + '\", '
    out = out + '\"nivel\" : \"' + tup[6] + '\", '
    out = out + '\"numero\" : \"' + str(tup[7]) + '\", '
    out = out + '\"letra\" : \"' + tup[8] + '\"'

    out = out + "}"
    print out
    return out
