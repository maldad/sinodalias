--run with: psql -d test, then inside psql: \i {filename}
-- Schema: test

DROP SCHEMA IF EXISTS test CASCADE;

CREATE SCHEMA test
  AUTHORIZATION maldad;
SET SEARCH_PATH to test;
-- Table: test.registros

DROP TABLE IF EXISTS test.registros CASCADE;

CREATE TABLE test.registros
(
  fecha date,
  folio integer NOT NULL,
  nombre character varying,
  matricula character varying,
  carrera character varying,
  concepto character varying,
  nivel character varying,
  importe_numero integer,
  importe_letra character varying,
  CONSTRAINT "folioPK" PRIMARY KEY (folio)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test.registros
  OWNER TO maldad;

-- Insert first row so we can start folios from it
INSERT INTO test.registros VALUES('2017/10/27', 0, 'No Name', 'No Matricula', 'No Studies', 'No Concept', 'No Degree', 0, 'No Cost');
