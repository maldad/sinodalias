-- run with: psql -f {filename}
-- Database: test

DROP DATABASE IF EXISTS test;

CREATE DATABASE test
  WITH OWNER = maldad
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'es_MX.UTF-8'
       LC_CTYPE = 'es_MX.UTF-8'
       CONNECTION LIMIT = -1;


