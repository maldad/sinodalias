var GUI = (function (){
  
  var _getElement = function(id){
    return document.getElementById(id);
  };

  var period = function(){
    var months = HTML.newElement("h3").setText("AGO - DIC 2017")
      .setId("period");
    var cont = HTML.newElement("div").addClass("pure-u-1-3")
      .addClass("header")
      .appendChild(months.element());
    return cont;
  };

  var sep = function(){
    var img = HTML.newElement("img").setAttribute("src", "static/img/sep.png")
      .setId("img1")
      .setAttribute("width", 300).setAttribute("height", 150);
    var cont = HTML.newElement("div")
      .addClass("header")
      .addClass("pure-u-1-3").appendChild(img.element());
    return cont;
  };

  var ito = function(){
    var img = HTML.newElement("img").setAttribute("src", "static/img/ito.png")
      .setId("img2")
      .setAttribute("width", 100).setAttribute("height", 100);
    var cont = HTML.newElement("div")
      .addClass("header")
      .addClass("pure-u-1-3").appendChild(img.element());
    return cont;
  };

  var _head = function(){
    var cont = HTML.newElement("div").addClass("pure-g");
    cont.appendChild(period().element());
    cont.appendChild(sep().element());
    cont.appendChild(ito().element());
    
    return cont;
  };

  var _form = function(){
    var form = HTML.newElement("form")
      .setId("form1")
      .addClass("pure-form");

    var d = new Date();
    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-2")
        .setAttribute("name", "fecha")
        .setAttribute("value", d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear())
        .setAttribute("placeholder", "Fecha: dd/mm/aa").element());

    form.appendChild(HTML.newElement("input")
        .setId("ifolio")
        .addClass("pure-input-1-2")
        .setAttribute("name", "folio")
        .setAttribute("readonly", "").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-3-4")
        .setAttribute("name", "nombre")
        .setAttribute("placeholder", "NOMBRE").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-4")
        .setAttribute("name", "matricula")
        .setAttribute("placeholder", "Matrícula").element());


    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-3")
        .setAttribute("value", "Carrera: ")
        .setAttribute("readonly", "").element());

    form.appendChild(HTML.newElement("select")
        .addClass("pure-input-2-3")
        .setAttribute("name", "carrera")
        .appendChild(HTML.newElement("option").setText("Ing. Electrónica").element())
        .appendChild(HTML.newElement("option").setText("Ing. Eléctrica").element())
        .appendChild(HTML.newElement("option").setText("Ing. Civil").element())
        .appendChild(HTML.newElement("option").setText("Ing. Mecánica").element())
        .appendChild(HTML.newElement("option").setText("Ing. Industrial").element())
        .appendChild(HTML.newElement("option").setText("Ing. Química").element())
        .appendChild(HTML.newElement("option").setText("Ing. Gestión Empresarial").element())
        .appendChild(HTML.newElement("option").setText("Ing. Sistemas Computacionales").element())
        .appendChild(HTML.newElement("option").setText("Lic. Administración").element())
        .element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-2")
        .setAttribute("value", "Concepto:")
        .setAttribute("readonly", "").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-2")
        .setAttribute("name", "concepto")
        .setAttribute("value", "Examen de titulación Sinodalias")
        .setAttribute("readonly", "").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-2")
        .setAttribute("value", "Nivel: ")
        .setAttribute("readonly", "").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-2")
        .setAttribute("name", "nivel")
        .setAttribute("readonly", "")
        .setAttribute("value", "Licenciatura")
        .element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-3")
        .setAttribute("value", "Importe  $: ")
        .setAttribute("readonly", "").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-3")
        .setAttribute("name", "numero")
        .setAttribute("value", "1000")
        .setAttribute("readonly", "")
        .setAttribute("placeholder", "Cantidad con número").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-3")
        .setAttribute("name", "letra")
        .setAttribute("value", "un mil pesos 00/100 MN")
        .setAttribute("readonly", "")
        .setAttribute("placeholder", "(Cantidad con letra)").element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-4")
        .setAttribute("name", "presidente")
        .setAttribute("placeholder", "Presidente")
        .element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-4")
        .setAttribute("name", "secretario")
        .setAttribute("placeholder", "Secretario")
        .element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-4")
        .setAttribute("name", "vocal1")
        .setAttribute("placeholder", "Vocal")
        .element());

    form.appendChild(HTML.newElement("input")
        .addClass("pure-input-1-4")
        .setAttribute("name", "vocal2")
        .setAttribute("placeholder", "Vocal")
        .element());

    return form;
  };

  var _acceptButton = function(){
    var b = HTML.newElement("button")
      .addClass("pure-button")
      .addClass("pure-button-primary")
      .setId("button1")
      .setAttribute("disabled", "")
      .setText("Aceptar");

    var url = "http://127.0.0.1:5000/post"
    var f = function(event){
      var args = Process.buildRegister();
      XHR.post(url, args);
      Process.generatePDF(args);
    };

    b.element().addEventListener("click", f, false);
    
    var d = HTML.newElement("div")
      .appendChild(b.element())
      .setId("centerButton");
    return d;
  };

  var _createGui = function(){
    console.log("creating the GUI");
    var container = _getElement("mainContainer");
    var panel = _getElement("mainPanel");
    var panel = HTML.newElement("div")
      .setId("mainPanel")
      .appendChild(_head().element())
      .appendChild(_form().element())
      .appendChild(_acceptButton().element())

    container.appendChild(panel.element());

    //adding events, nombre and matricula must contain text
    var inputName = GUI.getElement("form1")[2];
    var inputMatricula = GUI.getElement("form1")[3];
    var b = GUI.getElement("button1");
    console.log(b);

    inputName.addEventListener("blur", function(event){
      if(event.target.value == ""){
        event.target.style.background = "pink";
        b.setAttribute("disabled", "");
      }else{
        event.target.style.background = "";
        b.removeAttribute("disabled");
      }
    });

    inputMatricula.addEventListener("blur", function(event){
      if(event.target.value == ""){
        event.target.style.background = "pink";
        b.setAttribute("disabled", "");
      }else{
        event.target.style.background = "";
        b.removeAttribute("disabled");
      }
    });

    // var form = panel.children[1];
    // var f = function(input){
    //   return input.value = "testing, attention please";
    // };
    // HOF.map(f, form);

    XHR.get("http://127.0.0.1:5000/get");
  };

  var _fillFolio = function(folio){
    if(folio.length >= 4){
      return folio;
    }else{
      return _fillFolio("0" + folio);
    }
  };

  var _setFolio = function(folio){
    var inputFolio = _getElement("form1").children[1];
    var fol = parseInt(folio);
    inputFolio.value = _fillFolio(fol + 1);
  };

  return{
    "getElement" : _getElement,
    "createGui" : _createGui,
    "setFolio" : _setFolio
  };

})();
