from flask import Flask, render_template, url_for, request
import datetime
import database
app = Flask(__name__)

@app.route('/')
def index():
    number_of_records = database.queryNumberOfRecords()
    if(number_of_records >= 100):
        return "no more records are allowed, yet :)"
    else:
        return render_template('index.html')

@app.route('/post', methods=['POST'])
def post():
    now = datetime.datetime.now()
    print "POST: ", now.strftime("%Y/%m/%d %H:%M")
    jsonData = request.get_json()
    # print jsonData['fecha'], jsonData['folio'],jsonData['nombre'],jsonData['matricula'],jsonData['carrera'],jsonData['concepto'],jsonData['nivel'],jsonData['numero'],jsonData['letra']
    database.insertRegister(jsonData)
    return index()

@app.route('/get', methods=['GET'])
def get():
    folio = database.queryMaxFolio()
    return folio

@app.route('/registers', methods=['GET'])
def registers():
    return database.queryAll()
